package wiki.controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;
import wiki.modelo.bean.Artista;
import wiki.modelo.bean.Cancion;
import wiki.modelo.bean.Compositor;
import wiki.modelo.bean.jdbc.ArtistaJDBC;
import wiki.modelo.bean.jdbc.CancionJDBC;
import wiki.modelo.bean.jdbc.CompositorJDBC;

/**
 *
 * @author Hugo
 */
@WebServlet(name = "CrearCancionServlet", urlPatterns = {"/crearCancion"})
public class CrearCancionServlet extends HttpServlet {
    
    XmlWebApplicationContext spring;
    ArtistaJDBC artistaJDBC;
    CompositorJDBC compositorJDBC;
    CancionJDBC cancionJDBC;
    
    @Override
    public void init() {
        spring = (XmlWebApplicationContext) WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        artistaJDBC = (ArtistaJDBC) spring.getBean("artistaJDBC");
        compositorJDBC = (CompositorJDBC) spring.getBean("compositorJDBC");
        cancionJDBC = (CancionJDBC) spring.getBean("cancionJDBC");
    }

    /**
     * Recibe peticiones de tipo GET
     */
    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        List<Artista> artistas = artistaJDBC.listArtistas();
        List<Compositor> compositores = compositorJDBC.listCompositors();
        
        request.setAttribute("artistas", artistas);
        request.setAttribute("compositores", compositores);
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/wiki/crearCancion.jsp");
        dispatcher.forward(request, response);
    }

    /**
     * Recibe peticiones de tipo POST
     */
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String nombre = request.getParameter("nombre");
        String album = request.getParameter("album");
        String letra = request.getParameter("letra");
        String historia = request.getParameter("historia");
        Integer artistaId = Integer.valueOf(request.getParameter("artistaId"));
        Integer compositorId = Integer.valueOf(request.getParameter("compositorId"));
        
        Cancion cancion = new Cancion();
        cancion.setNombre(nombre);
        cancion.setAlbum(album);
        cancion.setLetra(letra);
        cancion.setHistoria(historia);
        cancion.setInterprete(new Artista(artistaId));
        cancion.setCompositor(new Compositor(compositorId));
        
        cancionJDBC.create(cancion);
        
        response.sendRedirect("/wiki2015/index.jsp");
    }
}
