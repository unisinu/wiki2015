package wiki.controlador;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.context.support.XmlWebApplicationContext;
import wiki.modelo.bean.Cancion;
import wiki.modelo.bean.jdbc.CancionJDBC;

@WebServlet(name = "VerCancionServlet", urlPatterns = {"/verCancion"})
public class VerCancionServlet extends HttpServlet {

    XmlWebApplicationContext spring;
    private CancionJDBC cancionJDBC;

    @Override
    public void init() {
        spring = (XmlWebApplicationContext) WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        cancionJDBC = (CancionJDBC) spring.getBean("cancionJDBC");
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        System.out.println("--------------------------------------------------");
        Integer id = Integer.valueOf(request.getParameter("id"));
        Cancion cancion = cancionJDBC.getCancion(id);
        request.setAttribute("cancion", cancion);
        RequestDispatcher dispatcher = request.getRequestDispatcher("/wiki/verCancion.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }
}
