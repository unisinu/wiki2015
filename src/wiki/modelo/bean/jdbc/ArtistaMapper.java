package wiki.modelo.bean.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import wiki.modelo.bean.Artista;

public class ArtistaMapper implements RowMapper<Artista> {

    @Override
    public Artista mapRow(ResultSet rs, int i) throws SQLException {
        Artista palabra = new Artista();
        palabra.setId(rs.getInt("id"));
        palabra.setNombres(rs.getString("nombres"));
        palabra.setApellidos(rs.getString("apellidos"));
        palabra.setNombreArtistico(rs.getString("nombre_artistico"));
        palabra.setGenero(rs.getString("genero"));
        palabra.setPerfil(rs.getString("perfil"));
        return palabra;
    }
}
