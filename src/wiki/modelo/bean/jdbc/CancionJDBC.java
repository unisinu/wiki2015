package wiki.modelo.bean.jdbc;

import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import wiki.modelo.bean.Cancion;

public class CancionJDBC {

    private JdbcTemplate jdbcTemplate;
    private CancionMapper mapper;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setMapper(CancionMapper mapper) {
        this.mapper = mapper;
    }

    public Cancion getCancion(Integer id) {
        StringBuilder sql = new StringBuilder();
        sql.append("select ca.id as ca_id,ca.nombre as ca_nombre,ca.album as ca_album,ca.letra as ca_letra,");
        sql.append("ca.historia as ca_historia,ar.id as ar_id,ar.nombres as ar_nombres,ar.apellidos as ar_apellidos,");
        sql.append("ar.nombre_artistico as ar_nombre_artistico,ar.genero as ar_genero,ar.perfil as ar_perfil,");
        sql.append("co.id as co_id,co.nombres as co_nombres,co.apellidos as co_apellidos ");
        sql.append("from canciones ca join artistas ar on ca.interprete = ar.id ");
        sql.append("join compositores co on ca.compositor = co.id WHERE ca.id = ? ORDER BY ca.nombre;");
        Cancion artista = jdbcTemplate.queryForObject(sql.toString(), new Object[]{id}, mapper);
        return artista;
    }

    public void create(Cancion cancion) {
        String SQL = "INSERT INTO canciones (nombre, album, interprete, compositor, letra, historia) VALUES (?, ?, ?, ?, ?, ?)";
        jdbcTemplate.update(SQL, cancion.getNombre(), cancion.getAlbum(), cancion.getInterprete().getId(),
                cancion.getCompositor().getId(), cancion.getLetra(), cancion.getHistoria());
    }

    public List<Cancion> listCanciones() {
        StringBuilder sql = new StringBuilder();
        sql.append("select ca.id as ca_id,ca.nombre as ca_nombre,ca.album as ca_album,ca.letra as ca_letra,");
        sql.append("ca.historia as ca_historia,ar.id as ar_id,ar.nombres as ar_nombres,ar.apellidos as ar_apellidos,");
        sql.append("ar.nombre_artistico as ar_nombre_artistico,ar.genero as ar_genero,ar.perfil as ar_perfil,");
        sql.append("co.id as co_id,co.nombres as co_nombres,co.apellidos as co_apellidos ");
        sql.append("from canciones ca join artistas ar on ca.interprete = ar.id ");
        sql.append("join compositores co on ca.compositor = co.id ORDER BY ca.nombre;");
        List<Cancion> artistas = jdbcTemplate.query(sql.toString(), mapper);
        return artistas;
    }

    public void delete(Integer id) {
        String SQL = "DELETE FROM artistas where id = ?";
        jdbcTemplate.update(SQL, id);
    }

    public void update(Cancion cancion) {
        String SQL = "UPDATE canciones SET nombres=?, album=?, interprete=?, compositor=?, letra=?, historia=? WHERE id = ?";
        jdbcTemplate.update(SQL, cancion.getNombre(), cancion.getAlbum(), cancion.getInterprete().getId(),
                cancion.getCompositor().getId(), cancion.getLetra(), cancion.getHistoria(), cancion.getId());
    }
}
