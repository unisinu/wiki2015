package wiki.modelo.bean.jdbc;

import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import wiki.modelo.bean.Compositor;

public class CompositorJDBC {

    private JdbcTemplate jdbcTemplate;
    private CompositorMapper mapper;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setMapper(CompositorMapper mapper) {
        this.mapper = mapper;
    }

    public Compositor getCompositor(Integer id) {
        String SQL = "SELECT * FROM compositores WHERE id = ?";
        Compositor compositor = jdbcTemplate.queryForObject(SQL,
                new Object[]{id}, mapper);
        return compositor;
    }

    public void create(Compositor compositor) {
        String SQL = "INSERT INTO compositores (nombres, apellidos) VALUES (?, ?)";
        jdbcTemplate.update(SQL, compositor.getNombres(), compositor.getApellidos());
    }

    public List<Compositor> listCompositors() {
        String SQL = "SELECT * FROM compositores";
        List<Compositor> compositores = jdbcTemplate.query(SQL, mapper);
        return compositores;
    }

    public void delete(Integer id) {
        String SQL = "DELETE FROM compositores where id = ?";
        jdbcTemplate.update(SQL, id);
    }

    public void update(Compositor artista) {
        String SQL = "UPDATE compositores SET nombres = ?, apellidos=? WHERE id = ?";
        jdbcTemplate.update(SQL, artista.getNombres(), artista.getApellidos(), artista.getId());
    }
}
