package wiki.modelo.bean.jdbc;

import java.util.List;
import org.springframework.jdbc.core.JdbcTemplate;
import wiki.modelo.bean.Artista;

public class ArtistaJDBC {

    private JdbcTemplate jdbcTemplate;
    private ArtistaMapper mapper;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public void setMapper(ArtistaMapper mapper) {
        this.mapper = mapper;
    }

    public Artista getArtista(Integer id) {
        String SQL = "SELECT * FROM artistas WHERE id = ?";
        Artista artista = jdbcTemplate.queryForObject(SQL,
                new Object[]{id}, mapper);
        return artista;
    }

    public void create(Artista artista) {
        String SQL = "INSERT INTO artistas (nombres, apellidos, nombre_artistico, genero, perfil) VALUES (?, ?, ?, ?, ?)";
        jdbcTemplate.update(SQL, artista.getNombres(), artista.getApellidos(), artista.getNombreArtistico(), artista.getGenero(), artista.getPerfil());
    }

    public List<Artista> listArtistas() {
        String SQL = "SELECT * FROM artistas";
        List<Artista> artistas = jdbcTemplate.query(SQL, mapper);
        return artistas;
    }

    public void delete(Integer id) {
        String SQL = "DELETE FROM artistas where id = ?";
        jdbcTemplate.update(SQL, id);
    }

    public void update(Artista artista) {
        String SQL = "UPDATE artistas SET nombres = ?, apellidos=?, nombre_artistico=?, genero=?, perfil=? WHERE id = ?";
        jdbcTemplate.update(SQL, artista.getNombres(), artista.getApellidos(), artista.getNombreArtistico(), artista.getGenero(), artista.getPerfil(), artista.getId());
    }
}
