package wiki.modelo.bean.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import wiki.modelo.bean.Artista;
import wiki.modelo.bean.Cancion;
import wiki.modelo.bean.Compositor;

public class CancionMapper implements RowMapper<Cancion> {

    @Override
    public Cancion mapRow(ResultSet rs, int i) throws SQLException {
        Cancion cancion = new Cancion();
        cancion.setId(rs.getInt("ca_id"));
        cancion.setNombre(rs.getString("ca_nombre"));
        cancion.setAlbum(rs.getString("ca_album"));
        cancion.setLetra(rs.getString("ca_letra"));
        cancion.setHistoria(rs.getString("ca_historia"));
        
        Artista interprete = new Artista();
        interprete.setId(rs.getInt("ar_id"));
        interprete.setNombres(rs.getString("ar_nombres"));
        interprete.setApellidos(rs.getString("ar_apellidos"));
        interprete.setNombreArtistico(rs.getString("ar_nombre_artistico"));
        interprete.setGenero(rs.getString("ar_genero"));
        interprete.setPerfil(rs.getString("ar_perfil"));
        
        Compositor compositor = new Compositor();
        compositor.setId(rs.getInt("co_id"));
        compositor.setNombres(rs.getString("co_nombres"));
        compositor.setApellidos(rs.getString("co_apellidos"));
        
        cancion.setInterprete(interprete);
        cancion.setCompositor(compositor);
        return cancion;
    }
}
