package wiki.modelo.bean.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import wiki.modelo.bean.Compositor;

public class CompositorMapper implements RowMapper<Compositor> {

    @Override
    public Compositor mapRow(ResultSet rs, int i) throws SQLException {
        Compositor compositor = new Compositor();
        compositor.setId(rs.getInt("id"));
        compositor.setNombres(rs.getString("nombres"));
        compositor.setApellidos(rs.getString("apellidos"));
        return compositor;
    }
}
