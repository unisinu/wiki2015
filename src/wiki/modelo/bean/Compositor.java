package wiki.modelo.bean;

import java.io.Serializable;

public class Compositor implements Serializable {
    private Integer id;
    private String nombres;
    private String apellidos;
    
    public Compositor() {
        
    }
    
    public Compositor(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }    
}
