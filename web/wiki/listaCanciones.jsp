<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Lista de Canciones</h1>
        <table width="100%">
            <tr>
                <td>Nombre</td>
                <td>Album</td>
                <td>Interprete</td>
                <td>compositor</td>
                <td></td>
            </tr>
            <c:forEach var="cancion" items="${canciones}">
                <tr>
                <td>${cancion.nombre}</td>
                <td>${cancion.album}</td>
                <td>${cancion.interprete.nombreArtistico}</td>
                <td>${cancion.compositor.nombres} ${cancion.compositor.apellidos}</td>
                <td>
                    <a href="verCancion?id=${cancion.id}">Ver</a>
                </td>
            </tr>
            </c:forEach>
        </table>
    </body>
</html>
