<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table width="100%">
            <tr>
                <td width="30%">
                    <table>
                        <tr>
                            <td><b>Nombre:</b></td>
                            <td>${cancion.nombre}</td>
                        </tr>
                        <tr>
                            <td><b>Album:</b></td>
                            <td>${cancion.album}</td>
                        </tr>
                        <tr>
                            <td><b>Interprete:</b></td>
                            <td>${cancion.interprete.nombreArtistico}</td>
                        </tr>
                        <tr>
                            <td><b>Compositor:</b></td>
                            <td>${cancion.compositor.nombres} ${cancion.compositor.apellidos}</td>
                        </tr>
                    </table>
                </td>
                <td width="70%">
                    <table>
                        <tr>
                            <td width="50%"><b>Letra</b></td>
                            <td width="50%">Historia</td>
                        </tr>
                        <tr>
                            <td width="50%">${cancion.letra}</td>
                            <td width="50%">${cancion.historia}</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>
