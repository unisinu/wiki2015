<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Crear Cancion</title>
    </head>
    <body>
        <form action="crearCancion" method="POST">
            <table>
                <tr>
                    <td>Nombre:</td>
                    <td><input type="text" style="width: 235px;" name="nombre" /></td>
                    <td>Album:</td>
                    <td><input type="text" style="width: 235px;" name="album" /></td>
                </tr>
                <tr>
                    <td>Interprete:</td>
                    <td>
                        <select style="width: 235px;" name="artistaId">
                            <c:forEach var="artista" items="${artistas}">
                                <option value="${artista.id}">${artista.nombreArtistico}</option>
                            </c:forEach>
                        </select>
                    </td>
                    <td>Compositor:</td>
                    <td>
                        <select style="width: 235px;" name="compositorId">
                            <c:forEach var="compositor" items="${compositores}">
                                <option value="${compositor.id}">${compositor.nombres} ${compositor.apellidos}</option>
                            </c:forEach>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td>Letra:</td>
                    <td>
                        <textarea name="letra" style="margin: 0px; height: 157px; width: 235px;"></textarea>
                    </td>
                    <td>Historia:</td>
                    <td>
                        <textarea name="historia" style="margin: 0px; height: 157px; width: 235px;"></textarea>
                    </td>
                </tr>
                
                
            </table>
            <input type="submit" value="Agregar" />
        </form>
    </body>
</html>
